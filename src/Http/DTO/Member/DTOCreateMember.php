<?php

namespace App\Http\DTO\Member;


use App\Http\DTO\RequestDTOInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\Request;

class DTOCreateMember implements RequestDTOInterface
{
    /**
     * @Assert\NotBlank()
     */
    private $address;

    /**
     * @Assert\NotBlank()
     */
    private $phone;

    /**
     * @Assert\NotBlank()
     */
    private $birthDate;

    /**
     * @Assert\NotBlank()
     */
    private $city;

    /**
     * @Assert\NotBlank()
     */
    private $state;

    /**
     * @Assert\NotBlank()
     */
    private $churchId;

    /**
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @Assert\NotBlank()
     */
    private $cpf;

    /**
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    public function __construct(Request $request)
    {
        $this->address = $request->get('address');
        $this->name = $request->get('name');
        $this->birthDate = $request->get('birthDate');
        $this->cpf = $request->get('cpf');
        $this->email = $request->get('email');
        $this->churchId = $request->get('churchId');
        $this->state = $request->get('state');
        $this->city = $request->get('city');
        $this->phone = $request->get('phone');

    }

    public function address(): string
    {
        return $this->address;
    }

    public function birthDate(): \DateTime
    {
        return \DateTime::createFromFormat('d/m/Y', $this->birthDate);
    }

    public function name(): string
    {
        return $this->name;
    }

    public function cpf(): string
    {
        return $this->cpf;
    }

    public function phone(): string
    {
        return $this->phone;
    }

    public function city(): string
    {
        return $this->phone;
    }

    public function state(): string
    {
        return $this->state;
    }

    public function churchId(): string
    {
        return $this->churchId;
    }


    public function email(): string
    {
        return $this->email;
    }
}
