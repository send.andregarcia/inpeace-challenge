<?php

namespace App\Http\Controller;

use App\Entity\Member;
use App\Form\MemberType;
use App\Http\DTO\Member\DTOCreateMember;
use App\Repository\ChurchRepository;
use App\Repository\MemberRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/member')]
class MemberController extends AbstractController
{

    #[Route('/', name: 'app_member_create', methods: ['POST'])]
    public function get(DTOCreateMember $request, MemberRepository $memberRepository, ChurchRepository $churchRepository): Response
    {
        $church = $churchRepository->find($request->churchId());
        $member = new Member();
        $member->setName($request->name());
        $member->setAddress($request->address());
        $member->setBirthDate($request->birthDate());
        $member->setCity($request->city());
        $member->setCpf($request->cpf());
        $member->setEmail($request->email());
        $member->setPhone($request->phone());
        $member->setState($request->state());
        $church->addMember($member);
        try {
            $memberRepository->add($member);
        } catch (OptimisticLockException|ORMException $e) {
            throw new HttpException(400, $e->getMessage());
        }
        return $this->json(['status' => true]);
    }

    #[Route('/{id}', name: 'app_member_show', methods: ['GET'])]
    public function show(Member $member): Response
    {
        return $this->render('member/show.html.twig', [
            'member' => $member,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_member_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Member $member, MemberRepository $memberRepository): Response
    {
        $form = $this->createForm(MemberType::class, $member);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $memberRepository->add($member);
            return $this->redirectToRoute('app_member_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('member/edit.html.twig', [
            'member' => $member,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_member_delete', methods: ['POST'])]
    public function delete(Request $request, Member $member, MemberRepository $memberRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$member->getId(), $request->request->get('_token'))) {
            $memberRepository->remove($member);
        }

        return $this->redirectToRoute('app_member_index', [], Response::HTTP_SEE_OTHER);
    }
}
