# InPeace Challenger

A [Docker](https://www.docker.com/)-based installer and runtime for the [Symfony](https://symfony.com) web framework, with full [HTTP/2](https://symfony.com/doc/current/weblink.html), HTTP/3 and HTTPS support.

![CI](https://github.com/dunglas/symfony-docker/workflows/CI/badge.svg)

## Rodando a aplicação

1. É necessário instalar o docker compose, [install Docker Compose](https://docs.docker.com/compose/install/)
2. Execute `docker-compose build --pull --no-cache` para fazer build de novas imagens
3. Run `docker-compose up` (para correr a aplicação e verificar os logs)
4. Abra `https://localhost` 
5. Execute `docker-compose down --remove-orphans` para parar os containeres

