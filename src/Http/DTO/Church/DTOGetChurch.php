<?php

namespace App\Http\DTO\Church;


use App\Http\DTO\RequestDTOInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\Request;

class DTOGetChurch implements RequestDTOInterface
{
    /**
     * @Assert\NotBlank()
     */
    private $id;

    public function __construct(Request $request)
    {
        $this->id = $request->get('id');
    }


    public function id(): int
    {
        return $this->id;
    }

}
