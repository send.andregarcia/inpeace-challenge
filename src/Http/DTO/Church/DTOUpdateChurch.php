<?php

namespace App\Http\DTO\Church;


use App\Http\DTO\RequestDTOInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\Request;

class DTOUpdateChurch implements RequestDTOInterface
{
    /**
     * @Assert\NotBlank()
     */
    private $id;

    /**
     * @Assert\NotBlank()
     */
    private $address;

    /**
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @Assert\NotBlank()
     * @Assert\Url()
     */
    private $website;

    /**
     * @Assert\NotBlank()
     * @Assert\Url()
     */
    private $image;

    public function __construct(Request $request)
    {
        $this->id = $request->get('id');
        $this->address = $request->get('address');
        $this->name = $request->get('name');
        $this->website = $request->get('website');
        $this->image = $request->get('image');
    }

    public function address(): string
    {
        return $this->address;
    }

    public function id(): int
    {
        return $this->id;
    }

    public function website(): string
    {
        return $this->website;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function image(): string
    {
        return $this->image;
    }
}
