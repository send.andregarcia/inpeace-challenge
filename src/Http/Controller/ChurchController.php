<?php

namespace App\Http\Controller;

use App\Entity\Church;
use App\Http\DTO\Church\DTOCreateChurch;
use App\Http\DTO\Church\DTODeleteChurch;
use App\Http\DTO\Church\DTOGetChurch;
use App\Repository\ChurchRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

class ChurchController extends AbstractController
{
    #[Route('/', name: 'index', methods: ['GET'])]
    public function index(ChurchRepository $churchRepository): Response
    {
        return $this->render('home.html.twig', ['churchs' => $churchRepository->findAll()]);
    }

    #[Route('/church/{id}', name: 'app_church', methods: ['GET'])]
    public function get(DTOGetChurch $request, ChurchRepository $churchRepository): Response
    {
        $church = $churchRepository->find($request->id());
        return $this->render('church.html.twig', ['church' => $church, 'members' => $church->getMembers()]);
    }

    #[Route('/church/{id}', name: 'app_update_church', methods: ['PUT'])]
    public function update(DTOGetChurch $request, ChurchRepository $churchRepository): Response
    {
        $church = $churchRepository->find($request->id());
        return $this->render('church.html.twig', ['church' => $church, 'members' => $church->getMembers()]);
    }

    /**
     *
     * @throws OptimisticLockException
     * @throws ORMException
     */
    #[Route('/church/{id}', name: 'app_delete_church', methods: ['DELETE'])]
    public function delete(DTODeleteChurch $request, ChurchRepository $churchRepository): Response
    {
        $church = $churchRepository->find($request->id());
        $churchRepository->remove($church);
        return $this->redirect('/home');
    }

    #[Route('/church', name: 'app_create_church', methods: ['POST'])]
    public function create(DTOCreateChurch $request, ChurchRepository $churchRepository): Response
    {

        $church = new Church();

        $church->setWebsite($request->website());
        $church->setName($request->name());
        $church->setAddress($request->address());
        $church->setImage($request->image());

        try {
            $churchRepository->add($church);
        } catch (OptimisticLockException|ORMException $e) {
            throw new HttpException(400, $e->getMessage());
        }

        return $this->json([
            'status' => true,
        ]);
    }
}
